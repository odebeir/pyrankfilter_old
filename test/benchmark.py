# -*- coding: utf-8 -*-
'''
Performance test for different tasks
'''
__author__ = 'Olivier Debeir'


from pyrankfilter.rankfilter import rankfilter,kernel_list
from pyrankfilter.helpers import timeit
from time import sleep,time
from scipy import misc


def benchmark_compilation():
    """Provides a quick and dirty benchmark for computation time evaluation

:param: no param
:returns: nothing

* a first run evaluate compilation+execution time

* a second run evaluate execution time only (compiled code in cache)
    """
    im = misc.imread('../test/data/cameraman.tif')
    fname = 'rank'
    print 'with compilation'
    start = time()
    for i in range(10):
        f = rankfilter(im,filtName = fname,radius = 3, force=True)
    timedelta = time()-start
    print 'enlapsed time for 10 filters : %d msec (%f msec each)'%(timedelta/10.0)

    print 'without compilation (using cached version)'
    start = datetime.datetime.now()
    for i in range(10):
        f = rankfilter(im,filtName = fname,radius = 3, force=False)
    timedelta = time()-start
    print 'enlapsed time for 10 filters : %d msec (%f msec each)'%(timedelta,timedelta/10.0)



def benchmark_filter():
    """Test function: evaluate the computation time for cell tracking
    """
    im = misc.imread('./data/cameraman.tif')
    stat_c = {}
    stat_f = {}
    for fname in kernel_list():
        print 'test: %s'%fname
        #compile
        start = time()
        f = rankfilter(im,filtName = fname,radius = 15,verbose = False,infSup=(.1,.9),spectral_interval=(10,10),force=True)
        compile_time = time()-start
        stat_c[fname] = compile_time

        #filter
        start = time()
        f = rankfilter(im,filtName = fname,radius = 15,verbose = False,infSup=(.1,.9),spectral_interval=(10,10),force=False)
        filter_time = time()-start
        stat_f[fname] = filter_time


    sort_compile = sorted(stat_c,key=stat_c.get)
    sort_filter = sorted(stat_f,key=stat_f.get)

    print '-'*20
    for f in sort_compile:
        print '%20s %6f(comp) %6f '%(f,stat_c[f],stat_f[f])
    print '-'*20
    for f in sort_filter:
        print '%20s %6f(comp) %6f '%(f,stat_c[f],stat_f[f])
    print '-'*20





if __name__ == "__main__":

#    benchmark_compilation()
    benchmark_filter()


#example of profiling using cprofile
#    import cProfile
#    cProfile.run('benchmark()','../../test/temp/fooprof')
#    import pstats
#    p = pstats.Stats('../../test/temp/fooprof')
#    p.strip_dirs().sort_stats('cumulative').print_stats()
#
#    klist = kernelList()
#    klist.sort()
#    for k in klist:
#        print k
#
