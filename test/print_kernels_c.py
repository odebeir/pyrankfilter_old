# -*- coding: utf-8 -*-
'''
Print all the available filter cores, uses auto_indent_c for a correct C indentation
'''
__author__ = 'Olivier Debeir'


from pyrankfilter.rankfilter import rankfilter,kernel_list,rankfiltercore
from pyrankfilter.helpers import auto_indent_c

if __name__ == "__main__":
    for test in kernel_list():
        print test
        print '-'*40
        print
        print '.. code-block:: c'
        print '    :linenos:\n'
        print '    //','-'*(40-len(test)/2),test,'-'*(40-len(test)/2)
        for line in auto_indent_c(rankfiltercore[test]).split('\n'):
            print '    ',line

