=============
Introduction
=============

Context description
-----------------------------
Rank filter are used in a variety of ways in image processing, for noise filtering, contrast enhancement,
but also specific point detection or threshold.

The underlying idea is that the pixels belonging to a certain neighborhood are ranked with respect to their gray value.
These filters are typically non-linear filters, since they cannot be obtained by linear operation on the neighborhood.

A local equalization is done on a complete image and a circular neighborhood like the example below:


.. code-block:: python

    from scipy import misc
    from pyrankfilter.rankfilter import rankfilter

    im = misc.imread('../test/data/cameraman.tif')
    f = rankFilter(im,filtName = 'egalise',radius = 30)

Algorithm
-----------------------------
The rank is computed using a moving histogram. New pixels entering the neighborhood are added to the local histogram,
pixels going out being subtracted from the distribution. A each step of the image scan, a process is then applied to
the local histogram. For example, the local maximum consist in returning the highest graylevel having a non zero value
in the histogram.

.. highlight:: c

.. code-block:: c

    // -------------------- maximum --------------------
    for(m=HISTOSIZE;(--m)&&(histo[m]==0););
    *ptemp=m;

or for the local morphological contrast enhancement, consisting in replacing each pixel by the local maximum OR the
local minimum depending of its closest original value.

.. code-block:: c

    // -------------------- morph_contr_enh --------------------
    for(m=HISTOSIZE;(--m)&&(histo[m]==0););
    max=m;
    for(m=0;(m<HISTOSIZE)&&(histo[m]==0);m++);
    min=m;
    if(abs(*p-min)<abs(*p-max))*ptemp=min;
    else*ptemp=max;

The rankfilter function
---------------------------------

The module ``pyrankfilter`` contains the essential of the filter function.

``pyrankfilter.ranfilter`` is the function that apply a rank filter on a 2D numpy array and
returns a filtered 2D numpy array.

A typical usage is given below:

.. code-block:: python

    import numpy as npy
    from pyrankfilter import rankfilter

    im = npy.zeros((100,100),dtype='uint8')
    im[:5,:5] = 255
    f = rankfilter(im,filtName = 'mean',radius = 30,verbose = False)


filtName
    The main parameter is the filter type that should be applied. A filter type defines the local processing that will
    be done on the local histogram. A complete list of filters is given here :ref:`filters`

    a list of available filters is given by the module function :
    ``pyrankfilter.rankfilter.kernelList()``

radius
    The radius defined the neighborhood used (circular shaped). The following example shows
    a morphological gradient of increasing size.

    .. plot:: code/ex4.py
        :width: 100%

infSup *[optional]*
    Tolerance to be used for some filters such as soft filters where one c an specify a inferior and a superior rank
    to used instead of using local absolute minimum and maximum.

spectral_interval *[optional]*
    R-The spectral interval (i.e. number of gray levels darker and brighter) to consider a pixel belonging to the pixel
    neighborhood. The default  is (5,5).

mask *[optional]*
    An optional mask parameter, an boolean image having the same size as the input image, defines the image area that
    will be filtered, mask pixels being False are set to 0.

    By default the complete image area is filtered, which means that even for large radii, the image borders are processed.

    The following example illustrates how a boolean mask specifies the processed area.

    .. plot:: code/ex5.py
        :width: 100%

verbose *[optional]*
    debug purpose

force *[optional]*
    force the re-compilation of the c-function, if False, the usual behaviour is used i.e. the function is compiled
    during the first call, every successive call of the same kernel will use the compiled code in cache
    (see weave module documentation for more details).