.. pyrankfilter documentation master file, created by
   sphinx-quickstart on Mon Apr 16 21:22:43 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

==========================================
Welcome to pyrankfilter's documentation
==========================================

About
==========================================

**Pyrankfilter** is a python module that implement rank filters on 2D images (numpy array).

Process is achieved using Weave inline compilation of C-code. The processing kernel is dynamically injected
in the filter, so it is possible to customize the kernel at run time.

The kernel is circular, with possibly large radius. The image border (i.e. pixels closer than a radius from the border)
are also filtered.

The main function implemented by the module is :

.. autofunction:: pyrankfilter.rankfilter.rankfilter()


Contents:
==========================================

.. toctree::
    :maxdepth: 2

    intro.rst
    download.rst
    gallery.rst
    modules.rst
    tests.rst
    filters.rst
    about.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

