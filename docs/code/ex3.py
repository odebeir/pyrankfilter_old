# -*- coding: utf-8 -*-
# apply a rank filter to a test image

import matplotlib.pyplot as plt
import numpy as npy

import context
from pyrankfilter import rankfilter


radius = 20
im = plt.imread('../../test/data/cameraman.tif')
auto = rankfilter(im,filtName = 'egalise',radius = radius)

plt.figure()
plt.imshow(auto,origin='upperleft',cmap=plt.cm.gray)
plt.xlabel('local equalization $r=%d$'%radius)

plt.show()
