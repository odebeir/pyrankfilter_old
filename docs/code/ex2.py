# -*- coding: utf-8 -*-
# apply a rank filter to a test image

import matplotlib.pyplot as plt
import numpy as npy

import context
from pyrankfilter import rankfilter


radius = 5
im = plt.imread('../../test/data/cameraman.tif')
th = rankfilter(im,filtName = 'threshold',radius = radius)

plt.figure()
plt.imshow(th,origin='upperleft',cmap=plt.cm.gray)
plt.xlabel('local treshold $r=%d$'%radius)

plt.show()
