# -*- coding: utf-8 -*-
# application to the watershed transform

import matplotlib.pyplot as plt
import numpy as npy
from scipy.ndimage.measurements import watershed_ift,label

import context
from pyrankfilter import rankfilter

im = plt.imread('../../test/data/cells.tif')

#denoising the input image
denoised = rankfilter(im,'spectral_median',20,spectral_interval=[10,5])

#computing the border image
gr = rankfilter(denoised,'tophat',1).astype(npy.uint8)

#identifying the objects of interest
cells_markers = rankfilter(im,'lowest',10,infSup=[10,5])

#applying the watershed transform
cells_labels = label(cells_markers)
markers = cells_labels[0]
ws = watershed_ift(gr,markers)

#extract the borders from the watershed result
borders = rankfilter(ws.astype('uint8'),'bottomhat',1)

im_marks = im
im_marks[borders>0] = 0

plt.figure(figsize=[10,10])
plt.subplot(2,2,1)
plt.imshow(denoised,origin='upperleft',cmap=plt.cm.gray)
plt.xlabel('denoised image')
plt.subplot(2,2,2)
plt.imshow(gr,origin='upperleft',cmap=plt.cm.gray)
plt.xlabel('image gradient''')
plt.subplot(2,2,3)
plt.imshow(markers,origin='upperleft')
plt.xlabel('markers')
plt.subplot(2,2,4)
plt.imshow(im_marks,origin='upperleft',cmap=plt.cm.gray)
plt.xlabel('watershed segmentation')


plt.show()
