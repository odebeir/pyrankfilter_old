# -*- coding: utf-8 -*-
# apply a rank filter to a test image

import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
import numpy as npy

import context
from pyrankfilter import rankfilter

ima = plt.imread('../../test/data/cameraman.tif')[200:320,300:480]

fig = plt.figure(figsize=[10,7])

lo = rankfilter(ima,filtName = 'lowest',radius = 10, infSup = [10,10])
hi = rankfilter(ima,filtName = 'highest',radius = 10, infSup = [10,10])
ext = rankfilter(ima,filtName = 'extrema',radius = 10, infSup = [10,10])
plt.subplot(2,2,1)
plt.imshow(ima,origin='upperleft',cmap=plt.cm.gray)
plt.xlabel('original')
plt.subplot(2,2,2)
plt.imshow(lo,origin='upperleft',cmap=plt.cm.gray)
plt.xlabel('local lowest')
plt.subplot(2,2,3)
plt.imshow(hi,origin='upperleft',cmap=plt.cm.gray)
plt.xlabel('local highest')
plt.subplot(2,2,4)
plt.imshow(ext,origin='upperleft',cmap=plt.cm.gray)
plt.xlabel('local extrema')
plt.show()
