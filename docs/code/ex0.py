# -*- coding: utf-8 -*-
# apply a rank filter to a test image

import matplotlib.pyplot as plt
import numpy as npy

import context
from pyrankfilter import rankfilter

name = 'median'
radius = 15
im = plt.imread('../../test/data/cameraman.tif')
f = rankfilter(im,filtName = name,radius = radius,infSup=(.1,.9))

im[:,im.shape[1]/2:] = f[:,im.shape[1]/2:]

plt.figure(figsize=[10,10])
plt.imshow(im,origin='upperleft',cmap=plt.cm.gray)
plt.title('%s ($r=%d$)'%(name,radius))

plt.show()
