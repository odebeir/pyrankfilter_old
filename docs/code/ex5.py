# -*- coding: utf-8 -*-
"""apply a rank filter to a test image
only the area in the mask is filtered
the filtered value outside the mask is not significant
"""

import matplotlib.pyplot as plt
import numpy as npy

import context
from pyrankfilter import rankfilter


im = plt.imread('../../test/data/cameraman.tif')

mask_image = npy.zeros_like(im,dtype=bool)
mask_image[200:300,100:250] = True

f = rankfilter(im,filtName = 'egalise',radius = 20, mask = mask_image)

result = im.copy()
result[mask_image] = f[mask_image]

plt.figure(figsize=[10,7])
plt.subplot(2,2,1)
plt.imshow(im,origin='upperleft',cmap=plt.cm.gray)
plt.xlabel('original')
plt.subplot(2,2,2)
plt.imshow(mask_image,origin='upperleft',cmap=plt.cm.gray)
plt.xlabel('mask')
plt.subplot(2,2,3)
plt.imshow(f,origin='upperleft',cmap=plt.cm.gray)
plt.xlabel('filtered')
plt.subplot(2,2,4)
plt.imshow(result,origin='upperleft',cmap=plt.cm.gray)
plt.xlabel('original + filtered')

plt.show()
