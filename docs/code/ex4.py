# -*- coding: utf-8 -*-
# apply a rank filter to a test image

import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
import numpy as npy

import context
from pyrankfilter import rankfilter

ima = plt.imread('../../test/data/cameraman.tif')[200:320,300:480]

f = 'gradient'
fig = plt.figure(figsize=[10,3])

grid2 = ImageGrid(fig, 111,
    nrows_ncols = (3, 5),
    direction="row",
    axes_pad = 0.05,
    add_all=True,
    label_mode = "1",
    share_all = True,
    cbar_location="right",
    cbar_mode="single",
    cbar_size="10%",
    cbar_pad=0.05,
)

grid2[0].set_xlabel("X")
grid2[0].set_ylabel("Y")

for ax, radius in zip(grid2, range(1,30,2)):
    auto = rankfilter(ima,filtName = f,radius = radius)
    im = ax.imshow(auto,origin='upperleft',cmap=plt.cm.gray)
    ax.text(50,10,'$r=%d$'%radius,color='r')
ax.cax.colorbar(im)
plt.show()
