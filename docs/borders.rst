
-----------------
Border detection
-----------------

Rank filter are wel suited for detecting borders (see gray level morphological operations).

The following example illustrate some of the filters that enhance image borders:

.. plot:: code/ex6.py
    :width: 100%



bottomhat
------------------------------

.. code-block:: c
    :linenos:

    // ------------------------------------ bottomhat ------------------------------------
     for(m=0;(m<HISTOSIZE)&&(histo[m]==0);m++);
     *ptemp=*p-m;


gradient
------------------------------

.. code-block:: c
    :linenos:

    // ------------------------------------ gradient ------------------------------------
     for(m=HISTOSIZE;(--m)&&(histo[m]==0););
     max=m;
     for(m=0;(m<HISTOSIZE)&&(histo[m]==0);m++);
     min=m;
     *ptemp=max-min;

tophat
------------------------------

.. code-block:: c
    :linenos:

    // ------------------------------------- tophat -------------------------------------
     for(m=HISTOSIZE;(--m)&&(histo[m]==0););
     *ptemp=m-*p;


soft_gradient
------------------------------

.. code-block:: c
    :linenos:

    // ---------------------------------- soft_gradient ----------------------------------
     sum=0;
     for(m=0;m<HISTOSIZE;m++){
         sum+=histo[m];
         if(sum>pop*inf){
             min=m;
             m=HISTOSIZE;
         }
     }
     sum=0;
     for(m=0;m<HISTOSIZE;m++){
         sum+=histo[m];
         if(sum>pop*sup){
             max=m;
             m=HISTOSIZE;
         }
     }
     f=(float)(max-min);
     *ptemp=f;

meansubstraction
------------------------------

.. code-block:: c
    :linenos:

    // -------------------------------- meansubstraction --------------------------------
     sum=0.0f;
     for(m=0;m<HISTOSIZE;m++)sum+=(m*histo[m]);
     *ptemp=(((float)*p-sum/pop)/2.0f+MEDIF);

soft_meansubstraction
------------------------------

.. code-block:: c
    :linenos:

    // ------------------------------ soft_meansubstraction ------------------------------
     sum=0;
     for(m=0;m<HISTOSIZE;m++){
         sum+=histo[m];
         if(sum>pop*inf){
             min=m;
             m=HISTOSIZE;
         }
     }
     sum=0;
     for(m=0;m<HISTOSIZE;m++){
         sum+=histo[m];
         if(sum>pop*sup){
             max=m;
             m=HISTOSIZE;
         }
     }
     sum=0;
     count=0;
     for(m=min;m<max;m++){
         sum+=(m*histo[m]);
         count+=histo[m];
     }
     if(pop){
         f=((float)(*p)-(float)(sum)/(float)count)/2.0f+MEDIF;
     }
     else{
         f=0;
     }
     *ptemp=f;

