.. _filters:

=============
Filters
=============



.. toctree::
    :maxdepth: 2

    borders.rst
    noise.rst
    detectors.rst
    contrast.rst
    threshold.rst
    spectral.rst





