===========
Gallery
===========

Image processing examples
------------------------------

* markers for the watershed transform

the rankfilter can be used to prepare markers for the watershed transform, the following example shows how to use it
with the watershed_ift() imported from scipy.ndimage.measurements.

.. plot:: code/ex7.py
    :width: 100%

* local image equalization

.. plot:: code/ex8.py
    :width: 100%

Notebooks for for ipython 0.12
--------------------------------

From ``ipython`` version 0.12 it is possible to interactively interact with a ipython kernel from a standard webclient.

The following files are notebook sessions compatible with ``ipython`` 0.12.


* a basic filter application: :download:`hello_world.ipynb <../notebook/hello_world.ipynb>`


.. note:: under construction
