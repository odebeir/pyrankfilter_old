About
================================

This project is a Python module for various 2D image rank filters.

Web
-----

Source repository: `Bitbucket <https://bitbucket.org/odebeir/pyrankfilter/>`_

Author's project website: `<http://homepages.ulb.ac.be/~odebeir/pyrankfilter>`_

License
---------

.. include:: license.rst


Image samples
---------------
Cameraman.tif can be found here : `Image Databases <http://www.ece.utk.edu/~gonzalez/ipweb2e/downloads/standard_test_images/standard_test_images.zip>`_

